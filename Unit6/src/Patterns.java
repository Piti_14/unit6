
public class Patterns {

	public static void main(String[] args) {
		printPatternA(10);
		System.out.println();
		printPatternB(10);
		System.out.println();
		printPatternC(10);
		System.out.println();
		printPatternD(10);
		System.out.println();
		printPatternE(10);
		System.out.println();
		printPatternF(10);
		System.out.println();
		printPatternG(10);
		System.out.println();
		printPatternH(10);
		System.out.println();
		printPatternI(10);
		System.out.println();
		printPatternJ(10);
		System.out.println();
		printPatternK(10);
		System.out.println();
		

	}
	
	private static void printPatternK(int size) {
		int size2 = size * 2;
		for(int row = 0; row < size; row++) {
			for(int col = 0; col < size2; col++) {
				if(col > size - row || col <= size + row) {
					System.out.print("# ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
	}
	
	// EJERCICIO T !!
	/*private static void printPatternK(int size) {
		int size2 = size * 2;
		for(int row = 0; row < size; row++) {
			for(int col = 0; col < size2; col++) {
				if(col <= size - row -1 || col >= size + row) {
					System.out.print("# ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
	}*/
	
	
	private static void printPatternJ(int size) {
		int size2 = size * 2;
		for(int row = 0; row < size; row++) {
			for(int col = 0; col < size2; col++) {
				if(col >= size2 - (size2 - row) && col < size2 - row - 1) {
					System.out.print("# ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
	}
	
	private static void printPatternI(int size) {
		for(int row = 0; row < size; row++) {
			for(int col = 0; col < size; col++) {
				if(row == 0 || row == size - 1 || col == 0 || col == size - 1 || col == row || col == size - row - 1) {
					System.out.print("# ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		
	}
	
	private static void printPatternH(int size) {
		for(int row = 0; row < size; row++) {
			for(int col = 0; col < size; col++) {
				if(row == 0 || row == size - 1 || col == row || col == size - row - 1) {
					System.out.print("# ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		
	}
	
	private static void printPatternG(int size) {
		for(int row = 0; row < size; row++) {
			for(int col = 0; col < size; col++) {
				if(row == 0 || row == size - 1 || col == size - row - 1) {
					System.out.print("# ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		
	}

	private static void printPatternF(int size) {
		for(int row = 0; row < size; row++) {
			for(int col = 0; col < size; col++) {
				if(row == 0 || row == size - 1 || col == row) {
					System.out.print("# ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}		
	}

	private static void printPatternE(int size) {
		for(int row = 0; row < size; row++) {
			for(int col = 0; col < size; col++) {
				if(row == 0 || row == size - 1 || col == 0 || col == size - 1) {
					System.out.print("# ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
	}	

	private static void printPatternD(int size) {
		for(int row = 0; row < size; row++) {
			for(int emptyCol = 0; emptyCol < size - row; emptyCol++) {
				System.out.print("  ");
			}
			for(int col = size - row; col < size; col++) {
				System.out.print("# ");
			}
			System.out.println();
		}		
	}
	
	private static void printPatternC(int size) {
		for(int row = 0; row < size; row++) {
			for(int col = row; col < size; col++) {
				System.out.print("# ");
			}
			System.out.println();
			for(int emptyCol = 0; emptyCol <= row; emptyCol++) {
				System.out.print("  ");
			}
		}		
	}
	
	private static void printPatternB(int size) {
		for(int row = 0; row < size ; row++) {
			for(int col = 0; col < size - row; col++) {
				System.out.print("# ");
			}
			System.out.println();
		}		
	}

	private static void printPatternA(int size) {
		for(int row = 0; row < size; row++) {
			for(int col = 0; col < row; col++) {
				System.out.print("# ");
			}
			System.out.println();
		}
	}
}
