import java.util.Calendar;

public class MainInvoice {

	public static void main(String[] args) {
		InvoiceLine i = new InvoiceLine(187, "hola", "Introduce aquí tu descripción", 1.50, 20);
		InvoiceLine i2 = new InvoiceLine(2, "adios", "Adios es lo contrario al saludo", 5007.2, 0);
	    InvoiceLine i3 = new InvoiceLine(80, "Nom_product", "Introduce aquí tu descripción", 90.09, 7);
	    InvoiceLine i4 = new InvoiceLine(11, "ProductoX", "Introduce aquí tu descripción", 102.76, 0);
	    
		Calendar today = Calendar.getInstance();
		Calendar due = setDate(2019, 3, 12);
		Calendar delivery = setDate(2019, 2, 27);
		String from = "CA Major Medical Institute 7802, Fast Delivery USS Enterprise Fox Park Avenue 37";
		String to = "AZ Natural Museum of Biology 6510, Quantum Robotics MedLabs Helm Street b/81";
		String sales = "Dr. Jordan McCollins";
		String shipTerm = "Due on receipt";
		String shipMethod = "UPS";
		String pay = "by Credit Card";
		
		Invoice in = new Invoice(852005, today, delivery, due, from, to, sales, shipMethod, shipTerm, pay, 6);
		in.addLine(i2);
		in.addLine(i);
		in.addLine(i4);
		in.addLine(i3);
		System.out.println(in.toString());
		
		

	}
	
	public static Calendar setDate(int year, int month, int day) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, month);
		c.set(Calendar.DAY_OF_WEEK, day);
		return c;
	}

}
