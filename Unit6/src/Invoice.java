import java.util.Calendar;
import java.text.DecimalFormat;

public class Invoice {
	public static final String HEAD ="|%-25s|%-16s|%-24s|%-16s|%-22s|%-12s|";
	public static final String FINAL_FOOTER = "%101s|%19s|";
	public static final String LINE = "--------------------------------------------------------------------------------------------------------------------------";
	public static final String MID_LINE = "-----------------------------------";
	public static final String MINI_LINE = "---------------------";
	public static final String HEADER= String.format(HEAD, "SALES PERSON", "SHIPPING METHOD", "SHIPPING TERMS", "DELIVERY DATE", "PAYMENT TERMS", "DUE DATE");
	public static final String FOOTER = "%87s|%13s|%19s|%n%87s" 
										+ MID_LINE + 
										"%n" + FINAL_FOOTER + "%n%101s" 
										+ MINI_LINE +
										"%n" + FINAL_FOOTER + "%n%101s" 
										+ MINI_LINE +
										"%n" + FINAL_FOOTER + "%n%101s" 
										+ MINI_LINE;
	private int numberInvoice;
	private Calendar date, deliverDate, dueDate; 
	private String origin, destiny, salesPerson, shippMethod,shippTerms, payMethod;
	private InvoiceLine[] lines;
	private int totalDiscount;
	private double discounted, subtotal, gst, total;
	private DecimalFormat myFormat = new DecimalFormat("###,###,###.00");
	
	public Invoice(int number, Calendar actualDate, Calendar deliveryDate, Calendar dueDate,
					String from, String to, String salePerson, String shippMethod, String terms, String payWith, int discount) {
		numberInvoice = number;
		date = actualDate;
		deliverDate = deliveryDate;
		this.dueDate = dueDate;
		origin = from;
		destiny = to;
		salesPerson = salePerson;
		this.shippMethod = shippMethod;
		shippTerms = terms;
		payMethod = payWith;
		totalDiscount = discount;
		lines = new InvoiceLine[30]; //By default is all NULL
		discounted = 0.00;
		subtotal = 0.00;
		gst = 0.00;
		total = 0.00;
	}
	
	public void addLine(InvoiceLine i) {
		int pos = 0;
		while(lines[pos] != null) {
			pos++;
		}
		lines[pos] = i;
	}
	
	public String dateToString(Calendar c) {
		String s = String.format("%tD", c); 
		// 
		return s;
	}
	public String todayDate(Calendar c) {
		String s = String.format("%tB %te, %tY", c, c, c);
		return s;
	}
	
	public String totalFinalLine() {
		double totalPrice = 0.00;
		int pos = 0;
		while(lines[pos] != null) {
			totalPrice += lines[pos].discountedPrice();
			pos++;
		}
		discounted = totalPrice * ((double)(totalDiscount) / 100);
		subtotal = totalPrice - discounted;
		gst = subtotal * 0.21; // VAT of 21%
		total = subtotal + gst;
		
		String s = String.format(FOOTER, "TOTAL DISCOUNT", (totalDiscount + "%"), String.format("$" + myFormat.format(discounted)), "",
				"SUBTOTAL", "$" + myFormat.format(subtotal), "",
				"GST", "$" + myFormat.format(gst), "",
				"TOTAL", "$" + myFormat.format(total), "");
		return s;
	}
	
	public String invoiceTop() {
		String s = "Invoice #" + numberInvoice + "\n						DATE: " + todayDate(date) + "\n"
				+"________________________________________________________________________________" + "\n\n";
		String trimmedOrigin1 = origin.substring(0, 27);
		String trimmedOrigin2 = origin.substring(27, 33);
		String trimmedOrigin3 = origin.substring(33, 62);
		String trimmedOrigin4 = origin.substring(62);
		
		String trimmedDestiny1 = destiny.substring(0, 28);
		String trimmedDestiny2 = destiny.substring(29, 34);
		String trimmedDestiny3 = destiny.substring(35, 58);
		String trimmedDestiny4 = destiny.substring(60);
		
		s += "FROM:						SHIP TO:\n"
				+ trimmedOrigin1 + "			" + trimmedDestiny1 + "\n"
				+ trimmedOrigin2 + "						" + trimmedDestiny2 + "\n"
				+ trimmedOrigin3 + "			" + trimmedDestiny3 + "\n"
				+ trimmedOrigin4 + "				" + trimmedDestiny4 + "\n";
		return s;
	}
	
	public String fullfillHeader() {
		String s = String.format(HEAD, salesPerson, shippMethod, shippTerms,
				dateToString(deliverDate), payMethod, dateToString(dueDate));
		return s;
	}
	
	public String toString() {
		String s = invoiceTop() + "\n" + LINE + "\n" + HEADER + "\n" + LINE +"\n"
				 + fullfillHeader() + "\n" + LINE + "\n\n" + InvoiceLine.header() + "\n";
		int pos = 0;
		while(lines[pos] != null) {
			s += lines[pos].line();
			pos++;
		}
		s += LINE + "\n" + totalFinalLine();
		return s;
	}
	
}
