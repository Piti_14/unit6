import java.text.DecimalFormat;

public class InvoiceLine {
	private String itemCode, description;
	private int quantity;
	private double price, discount, total;
	private DecimalFormat myFormat = new DecimalFormat("###,###.00");
	public static final String BORDER = "--------------------------------------------------------------------------------------------------------------------------";
	public static final String HEAD = "|%-6s|%-18s|%-46s|%13s|%13s|%19s|";
	public static final String INVOICE_HEADER = String.format(HEAD, "QTY", "#ITEM", "DESCRIPTION", "UNIT PRICE", "DISCOUNT", "LINE TOTAL");
	
	public InvoiceLine(int qty, String item, String description, double price, double discount) {
		itemCode = item;
		this.description = description;
		quantity = qty;
		this.price = price;
		this.discount = discount;
		total = quantity * price ;
	}
	
	public double discountedPrice() {
		if(discount == 0) {
			return total;			
		} else {
			total -= total * (discount / 100);
			return total;
		}
	}
	
	public String line() {
		String formattedDiscount = String.format("%.0f%%", this.discount);
		return String.format(HEAD, quantity, itemCode, description, "$" + myFormat.format(price), formattedDiscount, "$" + myFormat.format(total)) 
				+ "\n" + String.format(HEAD,"","","","","","") + "\n";
	}
	
	public static String header() {
		return BORDER + "\n" + INVOICE_HEADER + "\n" + BORDER;
	}
}
